package cn.jfast.framework.web.api;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import cn.jfast.framework.base.prop.CoreConsts;

public class HttpResponse extends HttpServletResponseWrapper {
	
	private final String CORS_ORIGIN = "Access-Control-Allow-Origin";
    private final String CORS_METHODS = "Access-Control-Allow-Methods";
    private final String CORS_MAX_AGE = "Access-Control-Max-Age";
    private final String CORS_HEADER = "Access-Control-Allow-Headers";
    
	public HttpResponse(HttpServletResponse response) {
		super(response);
		if(CoreConsts.cors_mode){
			setHeader(CORS_ORIGIN, "*");
	        setHeader(CORS_METHODS, "POST, GET, OPTIONS, DELETE, PUT, HEAD");
	        setHeader(CORS_MAX_AGE, "3600");
	        setHeader(CORS_HEADER, "x-requested-with");
		}
	}

}
