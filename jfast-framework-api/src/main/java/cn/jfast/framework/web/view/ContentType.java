/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view;

public enum ContentType {
    /**
     * Word doc
     */
    MSWORD("application/msword"),
    /**
     * Excel doc
     */
    MSEXCEL("application/vnd.ms-excel"),
    /**
     * PDF doc
     */
    PDF("application/pdf"),
    /**
     * text doc
     */
    TXT("text/plain"),
    /**
     * XML doc
     */
    XML("text/xml"),
    /**
     * PPT doc
     */
    MSPOWERPOINT("application/vnd.ms-powerpoint"),
    /**
     * Visio doc
     */
    VISIO("application/vnd.visio"),
    /**
     * Framemaker template
     */
    FRAMEMAKER("FRAMEMAKER"),
    /**
     * bmp image
     */
    BMP("image/bmp"),
    /**
     * gif image
     */
    GIF("image/gif"),
    /**
     * jpeg image
     */
    JPEG("image/jpeg"),
    /**
     * tiff image
     */
    TIFF("image/tiff"),
    DCX("image/x-dcx"),
    PCX("image/x-pcx"),
    AFP("application/afp"),
    RTF("application/rtf"),
    WORDPRO("application/vnd.lotus-wordpro"),
    WORDPERFECT("application/wordperfect5.1");

    private String contentType;

    public String getContentType() {
        return this.contentType;
    }

    ContentType(String contentType) {
        this.contentType = contentType;
    }

    ;
}
