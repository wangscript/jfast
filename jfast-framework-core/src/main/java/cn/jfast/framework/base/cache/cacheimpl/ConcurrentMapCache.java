/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base.cache.cacheimpl;

import cn.jfast.framework.base.cache.Cache;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 多段锁Map缓存
 * @author 泛泛o0之辈
 */
public class ConcurrentMapCache implements Cache {

	/**
	 * 空对象替身
	 */
	private static final Object NULL_HOLDER = new NullHolder();

	/**
	 * 缓存名
	 */
	private final String name;

	/**
	 * 实际缓存存储对象
	 */
	private final ConcurrentMap<Object, Object> store;

	/**
	 * 允许控制
	 */
	private final boolean allowNullValues;

	public ConcurrentMapCache(String name) {
		this(name, new ConcurrentHashMap<Object, Object>(256), true);
	}

	public ConcurrentMapCache(String name, boolean allowNullValues) {
		this(name, new ConcurrentHashMap<Object, Object>(256), allowNullValues);
	}

	public ConcurrentMapCache(String name, ConcurrentMap<Object, Object> store, boolean allowNullValues) {
		this.name = name;
		this.store = store;
		this.allowNullValues = allowNullValues;
	}

	public final String getName() {
		return this.name;
	}

	public final ConcurrentMap<Object, Object> getNativeCache() {
		return this.store;
	}

	public Object get(Object key) {
		return this.store.get(key);
	}

	public boolean containsKey(Object key) {
		return this.store.containsKey(key);
	}

	public final boolean isAllowNullValues() {
		return this.allowNullValues;
	}


	@SuppressWarnings("unchecked")
	public <T> T get(Object key, Class<T> type) {
		Object value = fromStoreValue(this.store.get(key));
		if (value != null && type != null && !type.isInstance(value)) {
			throw new IllegalStateException("存储值不符合想要的类型 [" + type.getName() + "]: " + value);
		}
		return (T) value;
	}

	public void put(Object key, Object value) {
		this.store.put(key, toStoreValue(value));
	}

	public void clear() {
		this.store.clear();
	}

	protected Object fromStoreValue(Object storeValue) {
		if (this.allowNullValues && storeValue == NULL_HOLDER) {
			return null;
		}
		return storeValue;
	}

	protected Object toStoreValue(Object userValue) {
		if (this.allowNullValues && userValue == null) {
			return NULL_HOLDER;
		}
		return userValue;
	}
	
	@SuppressWarnings("serial")
	private static class NullHolder implements Serializable {
	}

	public int size() {
		return this.store.size();
	}

}
