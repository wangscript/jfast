/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import cn.jfast.framework.jdbc.log.Connection;

import java.sql.SQLException;

/**
 * Connect工厂,获得当前线程的Connection代理对象
 *
 * @author 泛泛o0之辈
 */
public class ConnectionFactory {

    private  static  final ThreadLocal<java.sql.Connection> threadLocal = new ThreadLocal<java.sql.Connection>();

    /**
     * 获得当前线程下的Connection对象
     *
     * @return
     */
    public synchronized static java.sql.Connection getThreadLocalConnection() throws ClassNotFoundException, SQLException {
        java.sql.Connection conn = threadLocal.get();
        if (null == conn || conn.isClosed()){
            conn = Connection.getConnection(rebulidDataSource());
            threadLocal.set(conn);
        }
        conn.setAutoCommit(true);
        return conn;

    }

    /**
     * 销毁当前线程的Connection对象
     */
    public static void removeThreadLocalConnection() {
        threadLocal.remove();
    }

    /**
     * 重新初始化数据源
     */
    private static java.sql.Connection rebulidDataSource() throws SQLException, ClassNotFoundException {
        return DatabaseConfiguration.configure();
    }
}
