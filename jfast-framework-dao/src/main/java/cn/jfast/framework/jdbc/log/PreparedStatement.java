/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.log;

import java.lang.reflect.Proxy;

public class PreparedStatement {

	private static java.sql.PreparedStatement ps;
	private static SqlLogger handler;

	public static java.sql.PreparedStatement getPreparedStatement(java.sql.PreparedStatement ps,String sql,String targetDao,boolean noLog) {
		handler = new SqlLogger(ps,sql,targetDao,noLog);
		PreparedStatement.ps = (java.sql.PreparedStatement) Proxy.newProxyInstance(ps.getClass()
				.getClassLoader(), new Class[]{java.sql.PreparedStatement.class}, handler);
		return PreparedStatement.ps;
	}

}
