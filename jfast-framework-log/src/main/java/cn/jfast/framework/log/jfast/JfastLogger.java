/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.log.jfast;

import cn.jfast.framework.log.Logger;
import cn.jfast.framework.log.StatusLogger;

import java.util.logging.Level;

public class JfastLogger extends StatusLogger implements Logger {

	private java.util.logging.Logger log;

	private String className;

	public JfastLogger(String className) {
		this.className = className;
		this.log = java.util.logging.Logger.getLogger(className);
	}

	public JfastLogger(Class<?> clazz) {
		this.className = clazz.getName();
		this.log = java.util.logging.Logger.getLogger(this.className);
	}

	public void info(String message) {
		log.logp(Level.INFO, className,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				message);
	}

	public void info(String message, Throwable throwable) {
		log.logp(Level.INFO, className,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				message, throwable);
	}

	public void info(String message, Object... matchers) {
		this.info(String.format(message, matchers));
	}

	public void debug(String message) {
		log.logp(Level.FINE, className,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				message);
	}

	public void debug(String message, Throwable throwable) {
		log.logp(Level.FINE, className,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				message, throwable);
	}

	public void debug(String message, Object... matchers) {
		this.debug(String.format(message, matchers));
	}

	public void warn(String message) {
		log.logp(Level.WARNING, className, Thread.currentThread()
				.getStackTrace()[1].getMethodName(), message);
	}

	public void warn(String message, Throwable throwable) {
		log.logp(Level.WARNING, className, Thread.currentThread()
				.getStackTrace()[1].getMethodName(), message, throwable);
	}

	public void warn(String message, Object... matchers) {
		this.warn(String.format(message, matchers));
	}

	public void error(String message) {
		log.logp(Level.SEVERE, className, Thread.currentThread()
				.getStackTrace()[1].getMethodName(), message);
	}

	public void error(String message, Throwable throwable) {
		log.logp(Level.SEVERE, className, Thread.currentThread()
				.getStackTrace()[1].getMethodName(), message, throwable);
	}

	public void error(String message, Object... matchers) {
		this.error(String.format(message, matchers));
	}

	public String getName() {
		return this.className;
	}

	public Object getNativeLogger() {
		return this.log;
	}

	public void log(cn.jfast.framework.log.Level level, String message) {
		switch (level) {
		case INFO:
			this.info(message);
			break;
		case DEBUG:
			this.debug(message);
			break;
		case WARN:
			this.warn(message);
			break;
		case ERROR:
			this.error(message);
			break;
		}
	}

	public void log(cn.jfast.framework.log.Level level, String message,
			Throwable throwable) {
		switch (level) {
		case INFO:
			this.info(message, throwable);
			break;
		case DEBUG:
			this.debug(message, throwable);
			break;
		case WARN:
			this.warn(message, throwable);
			break;
		case ERROR:
			this.error(message, throwable);
			break;
		}
	}

	public void log(cn.jfast.framework.log.Level level, String message,
			Object... matchers) {
		this.log(level, String.format(message, matchers));
	}

}
