package cn.jfast.plugin.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import cn.jfast.plugin.wizards.AutoGenWizard;

public class AutoGenerateCodeAction implements IWorkbenchWindowActionDelegate {

	private IWorkbenchWindow window;
	@Override
	public void run(IAction action) {
		 AutoGenWizard wizard = new AutoGenWizard();
		 WizardDialog dialog = new WizardDialog(window.getShell(),wizard);
		 dialog.open() ;
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

}
