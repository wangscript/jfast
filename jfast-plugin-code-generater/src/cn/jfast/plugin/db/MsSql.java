/**
 * 
 */
package cn.jfast.plugin.db;

/**
 * MySql数据对象
 * 
 * @author jfast 2015年8月18日
 */
public class MsSql extends DbBase{

	private static MsSql instance = null;

	static {
		try {
			Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static MsSql instance() {
		if (instance == null) {
			instance = new MsSql();
		}
		return instance;
	}

	private MsSql() {
		super();
	}
	
	public static void main(String[] args) {
		try {
			Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
