package cn.jfast.plugin.wizards.provider;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.TableItem;

import cn.jfast.plugin.db.MySql;
import cn.jfast.plugin.db.Table;

public class CellModifyer implements ICellModifier {
	private TableViewer tv;

	public CellModifyer(TableViewer tv) {
		super();
		this.tv = tv;
	}

	@Override
	public boolean canModify(Object arg0, String arg1) {

		return true;
	}

	@Override
	public Object getValue(Object o, String p) {
		Table col = (Table) o;
		if (p.equals("tableName")) {
			return col.getTableName();
		} else if (p.equals("nickName")) {
			return col.getNickName();
		}
		return null;
	}

	@Override
	public void modify(Object e, String p, Object v) {
		TableItem it = (TableItem) e;
		Table col = (Table) it.getData();
		if (p.equals("nickName")) {
			col.setNickName((String) v);
			MySql.instance().getTable(col.getTableName()).setNickName((String)v);
		}
		tv.update(it, null);
		tv.refresh();
	}

}
