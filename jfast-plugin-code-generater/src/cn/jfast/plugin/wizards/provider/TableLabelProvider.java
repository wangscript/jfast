package cn.jfast.plugin.wizards.provider;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import cn.jfast.plugin.db.Table;

/**
 * 设置标签类
 * 
 * @author jfast 
 * 2015年8月18日
 */
public class TableLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	public String getColumnText(Object element, int columnIndex) {
		Table table = (Table) element;
		if (columnIndex == 0)
			return table.getTableName();
		else if (columnIndex == 1)
			return table.getNickName();
		else if (columnIndex == 2)
			return table.getRemarks();
		return "";

	}

	@Override
	public Image getColumnImage(Object arg0, int arg1) {
		return null;
	}

}
