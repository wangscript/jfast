package ${daoPkg};

import ${modelPkg}.${nickName?cap_first};
import ${daoPkg}.${nickName?cap_first}Dao;
import cn.jfast.framework.jdbc.annotation.Dao;

<#assign gentime = .now>
/**
 * ${nickName}模块对应的数据接口
 * @since ${gentime?string['yyyy-MM-dd HH:mm']}
 * @author jfast
 */
@Dao(name="${nickName?uncap_first}Dao")
public class ${nickName?cap_first}DaoImpl implements ${nickName?cap_first}Dao{

	/**
	 * 查询${nickName}详情
	 */
	public ${nickName?cap_first} get${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate}){
		return null;
	}
	
	/**
	 * 删除${nickName}对象
	 */
	public void delete${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate}){
		
	}
	
	/**
	 * 更新${nickName}对象
	 */
	public void update${nickName?cap_first}By${primaryKeyTemplate}(${nickName?cap_first} ${nickName?uncap_first}){
		
	}
	
	/**
	 * 添加${nickName}对象
	 */
	public void add${nickName?cap_first}(${nickName?cap_first} ${nickName?uncap_first}){
		
	}

}